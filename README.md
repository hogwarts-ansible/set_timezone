# set_timezone

This role sets the name of the host based on set_timezone_timezone variable, if
defined. Otherwise, Europe/Paris will be used.

# Example Playbook

```yaml
---
- name: Set timezone
  hosts: "all"
  roles:
    - role: set_timezone
```

# License

GPLv3

# Author information

Wallun <wallun AT disroot DOT org>
